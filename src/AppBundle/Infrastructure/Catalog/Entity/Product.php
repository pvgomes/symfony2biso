<?php
/**
 * Created by PhpStorm.
 * User: pvgomes
 * Date: 3/23/15
 * Time: 4:07 PM
 */

namespace AppBundle\Infrastructure\Catalog\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $sku;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $externalSku;

    /**
     * @var array
     */
    protected $externalAttributes;

    public function __construct($id, $name, $sku, $externalSku)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sku = $sku;
        $this->externalSku = $externalSku;
    }

    /**
     * @return mixed
     */
    public function getExternalSku()
    {
        return $this->externalSku;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return array
     */
    public function getExternalAttributes()
    {
        return $this->externalAttributes;
    }

    /**
     * @param array $externalAttributes
     */
    public function setExternalAttributes($externalAttributes)
    {
        $this->externalAttributes = $externalAttributes;
    }

}