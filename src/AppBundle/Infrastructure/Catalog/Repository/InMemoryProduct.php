<?php
/**
 * Created by PhpStorm.
 * User: pvgomes
 * Date: 3/23/15
 * Time: 3:54 PM
 */

namespace AppBundle\Infrastructure\Catalog\Repository;

use AppBundle\Domain\Catalog\Repository\Product as IRepository;
use AppBundle\Domain;
use AppBundle\Infrastructure;
class InMemoryProduct implements IRepository{

    private $products;

    public function __construct()
    {
        $this->products["MA048AP14WXLTRI-296312"] = new Infrastructure\Catalog\Entity\Product(1, "Barbie Dican", "MA048AP14WXLTRI-296312", "ABCDE-2014");
        $this->products["MA048AP14WXLTRI-296314"] = new Infrastructure\Catalog\Entity\Product(2, "He-man Dican", "MA048AP14WXLTRI-296312", "ABCDE-2015");
    }

    /**
     * Get Product By Sku
     * @param $sku
     * @return
     */
    public function getBySku($sku)
    {
        $product = $this->getDoctrineBySku($sku);
        if ($product instanceof Infrastructure\Catalog\Entity\Product) {
            $product->setExternalAttributes($this->getProductBySimpleSku($sku));
        }
        return Domain\Catalog\DataMapper\Product::getInstance()->assign($product);
    }


    protected function getDoctrineBySku($sku)
    {
        return $this->products[$sku];
    }

    /**
     * Alice Redis Simulation
     */
    protected function getProductBySimpleSku($sku)
    {
        return array (
            'meta' =>
                array (
                    'sku' =>  'MA048AP14WXLTRI' ,
                    'id_catalog_config' =>  '85785' ,
                    'attribute_set_id' =>  '2' ,
                    'name' =>  'Body Malwee Infantil Amarelo' ,
                    'weight' =>  '0.19' ,
                    'height' =>  '28' ,
                    'width' =>  '21' ,
                    'length' =>  '3' ,
                    'attribute_config_shipment_type_name' =>  'Próprio' ,
                    'attribute_config_shipment_type_id' =>  '1' ,
                    'description' =>  '<h2> O Body Malwee Infantil Amarelo é fresquinho e supermacio. Olha só! </h2><p /><p /><br /><ul><strong>Principais Características:</strong><p /><p /><li> Modelo liso; </li><li> Manga curta; </li><li> Abertura envelope nos ombros; </li><li> Fecho em botões de pressão nas pernas; </li><li> Composição: tecido <i>cotton light</i>; </li><li> Lavável à máquina/ Não deixar de molho/ Lavar com cores similares.</li></ul></br> Para garantir aquela gostosa sensação de bem-estar aos bebês, nada melhor que este <i>body</i> da <strong>Malwee</Strong>. Confeccionado em tecido leve e de qualidade incomparável, vem com fecho em botões de pressão nas pernas, facilitando no momento da troca. É perfeito! <br /><br />' ,
                    'activated_at' =>  '2014-09-05 17:49:29' ,
                    'updated_at' =>  '2014-09-05 17:50:15' ,
                    'grouped_products' =>  'MA048AP14WXLTRI|MA048AP83WYQTRI|MA048AP80WYTTRI' ,
                    'categories' =>  '2##|##868##|##1086' ,
                    'brand' =>  'Malwee' ,
                    'config_shipment_type' =>  'Próprio' ,
                    'config_shipment_type_position' =>  '0' ,
                    'inbound_shipment_type' =>  'Somente Fornecedor (CIF)' ,
                    'inbound_shipment_type_position' =>  '0' ,
                    'inbound_ipi' =>  '0.00' ,
                    'inbound_st' =>  '0.00' ,
                    'inbound_icms' =>  '12.00' ,
                    'color_family' =>  'Amarelo' ,
                    'gender' =>  'Masculino' ,
                    'gender_position' =>  '0' ,
                    'categorization_one' =>  'Roupas/Bodies' ,
                    'product_type' =>  'Body' ,
                    'age_to' =>  '1' ,
                    'age_to_position' =>  '0' ,
                    'vpc' =>  '0.00' ,
                    'ocasiao_vestuario_position' =>  '0' ,
                    'colecao_position' =>  '2' ,
                    'nome_colecao' =>  'Primavera/Verão 2014' ,
                    'main_category' =>  '868' ,
                    'config_id' =>  '85785' ,
                    'max_price' =>  '29.90' ,
                    'price' =>  '29.90' ,
                    'max_original_price' => null,
                    'original_price' => null,
                    'max_special_price' => null,
                    'special_price' => null,
                    'max_saving_percentage' => null,
                    'special_price_comparison' => null,
                    'quantity' => 18,
                    'free_shipping_rule' => null),
            'attributes' =>
                array (
                    'description' =>  '<h2> O Body Malwee Infantil Amarelo é fresquinho e supermacio. Olha só! </h2><p /><p /><br /><ul><strong>Principais Características:</strong><p /><p /><li> Modelo liso; </li><li> Manga curta; </li><li> Abertura envelope nos ombros; </li><li> Fecho em botões de pressão nas pernas; </li><li> Composição: tecido <i>cotton light</i>; </li><li> Lavável à máquina/ Não deixar de molho/ Lavar com cores similares.</li></ul></br> Para garantir aquela gostosa sensação de bem-estar aos bebês, nada melhor que este <i>body</i> da <strong>Malwee</Strong>. Confeccionado em tecido leve e de qualidade incomparável, vem com fecho em botões de pressão nas pernas, facilitando no momento da troca. É perfeito! <br /><br />' ,
                    'product_contents' =>  '1 Body' ,
                    'model' =>  'Infantil' ,
                    'product_weight' =>  '1.000' ,
                    'short_description' =>  '.' ,
                    'customer_service_phone' =>  '11 4005.1093' ,
                    'customer_service_email' =>  'atendimento@tricae.com.br' ,
                    'ocasiao_vestuario' =>  'Casual' ,
                    'colecao' =>  'Ano Todo' ),
            'supplier_id' =>  '134' ,
            'simples' =>
                array (
                    'MA048AP14WXLTRI-296312' =>
                        array (
                            'meta' =>
                                array (
                                    'sku' =>  'MA048AP14WXLTRI-296312' ,
                                    'id_catalog_simple' =>  '296312' ,
                                    'price' =>  '29.90' ,
                                    'quantity' =>  '3' ,
                                    'cif_cost' =>  '12.35' ,
                                    'barcode_ean' =>  '7909206085930' ,
                                    'size' =>  'RN' ,
                                    'size_position' =>  '0' ,
                                    'transport_type' =>  'Freight' ,
                                    'transport_type_position' =>  '1' ,
                                    'supplier_code' =>  '47012-1739'
                                ),
                            'attributes' => ""
                        ),
                    'MA048AP14WXLTRI-296313' =>
                        array (
                            'meta' =>
                                array (
                                    'sku' =>  'MA048AP14WXLTRI-296313' ,
                                    'id_catalog_simple' =>  '296313' ,
                                    'price' =>  '29.90' ,
                                    'quantity' =>  '3' ,
                                    'cif_cost' =>  '12.35' ,
                                    'barcode_ean' =>  '7909206085961' ,
                                    'size' =>  'P' ,
                                    'size_position' =>  '5' ,
                                    'transport_type' =>  'Freight' ,
                                    'transport_type_position' =>  '1' ,
                                    'supplier_code' =>  '47012-1739' ),
                            'attributes' => "",
                        ),
                    'MA048AP14WXLTRI-296314' =>
                        array (
                            'meta' =>
                                array (
                                    'sku' =>  'MA048AP14WXLTRI-296314' ,
                                    'id_catalog_simple' =>  '296314' ,
                                    'price' =>  '29.90' ,
                                    'quantity' =>  '6' ,
                                    'cif_cost' =>  '12.35' ,
                                    'barcode_ean' =>  '7909206085992' ,
                                    'size' =>  'M' ,
                                    'size_position' =>  '6' ,
                                    'transport_type' =>  'Freight' ,
                                    'transport_type_position' =>  '1' ,
                                    'supplier_code' =>  '47012-1739' ,
                                    'attributes' => ''
                                ),
                        ),
                    'MA048AP14WXLTRI-296315' =>
                        array (
                            'meta' =>
                                array (
                                    'sku' =>  'MA048AP14WXLTRI-296315' ,
                                    'id_catalog_simple' =>  '296315' ,
                                    'price' =>  '29.90' ,
                                    'quantity' =>  '6' ,
                                    'cif_cost' =>  '12.35' ,
                                    'barcode_ean' =>  '7909206086029' ,
                                    'size' =>  'G' ,
                                    'size_position' =>  '7' ,
                                    'transport_type' =>  'Freight' ,
                                    'transport_type_position' =>  '1' ,
                                    'supplier_code' =>  '47012-1739' ),
                            'attributes' => ""
                        ),
                ),
            'images' =>
                array (
                    0 =>
                        array (
                            'image' =>  '1' ,
                            'main' =>  '1' ,
                            'original_filename' =>  'MA048AP14WXLTRI_1.jpg' ,
                            'name' =>  'Body Malwee Infantil Amarelo' ,
                            'sku' =>  'MA048AP14WXLTRI' ,
                            'url' =>  '/p/Malwee-Body-Malwee-Infantil-Amarelo-6562-58758-1' ,
                            'path' =>  '/vagrant/bob/data/media/product/58/758/1.jpg' ,
                            'sprite' =>  '/p/Malwee-Body-Malwee-Infantil-Amarelo-6562-58758-sprite.jpg' ),
                    1 =>
                        array (
                            'image' =>  '2' ,
                            'main' =>  '0' ,
                            'original_filename' =>  'MA048AP14WXLTRI_2.jpg' ,
                            'name' =>  'Body Malwee Infantil Amarelo' ,
                            'sku' =>  'MA048AP14WXLTRI' ,
                            'url' =>  '/p/Malwee-Body-Malwee-Infantil-Amarelo-6563-58758-2' ,
                            'path' =>  '/vagrant/bob/data/media/product/58/758/2.jpg' )),
            'image' =>  '/p/Malwee-Body-Malwee-Infantil-Amarelo-6562-58758-1' ,
            'sprite' =>  '/p/Malwee-Body-Malwee-Infantil-Amarelo-6562-58758-sprite.jpg' ,
            'link' =>  'body-malwee-infantil-amarelo-85785.html' ,
        );
    }

} 