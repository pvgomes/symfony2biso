<?php
/**
 * Created by PhpStorm.
 * User: pvgomes
 * Date: 3/23/15
 * Time: 12:04 PM
 */

namespace AppBundle\Infrastructure\Catalog\Repository;

use AppBundle\Domain\Catalog\Repository\Product as IRepository;
use AppBundle\Domain;
use AppBundle\Infrastructure;

class Product implements IRepository{


    private $redisRepository;
    private $doctrineRepository;

    public function __construct()
    {
        $this->doctrineRepository = "";
        $this->redisRepository = "";
    }

    /**
     * Get Product By Sku
     * @param $sku
     * @return
     */
    public function getBySku($sku)
    {
        $product = $this->doctrineRepository->getBySku($sku);
        if ($product instanceof Infrastructure\Catalog\Entity\Product) {
            $product->setExternalAttributes($this->redisRepository->getBySku($sku));
        }
        return Domain\Catalog\DataMapper\Product::getInstance()->assign($product);
    }

} 