<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/app/poc", name="poc")
     */
    public function pocAction()
    {
        $a = new \Domain\Catalog\Entity\Product();
        $product = $this->get('repository.product')->getBySku('MA048AP14WXLTRI-296312');
        die(var_dump($product));
    }
}
