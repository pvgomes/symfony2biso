ExternalShop
============

## Instalação e configuração do Projeto

**1) Baixe o projeto do repositorio**:

```
git clone git@github.com:tricae-br/externalshop.git
```

**2) Baixe os pacotes do Composer**:

```
php composer.phar install
```

*Obs: use c comando "install" para não alterar a versão do arquivo "composer.lock"*

**3) Forneça os parametros do projeto**:

```
database_driver (pdo_mysql):
database_host (127.0.0.1): mariadb
database_port (null):
database_name (symfony): externalshop
database_user (root): root
database_password (null): root
mailer_transport (smtp):
mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
locale (en):
secret (ThisTokenIsNotSoSecretChangeIt): coloque_aqui_uma_hash_segura
```

*Obs: Manter os campos vazios para uso dos valores padrão*

## Instalação do Ambiente

**1) Utilizando Docker + Docker-Compose**:

```
docker-compose up -d
```

**2) Adicione o IP do container no seu "hosts"**:

```
# Show Nginx Container IP
docker inspect --format='{{ .NetworkSettings.IPAddress }}' externalshop_nginx_1
# Add Nginx IP to Hosts
echo "172.17.0.2 externalshop.dev" >> /etc/hosts
```

**3) Acesse pelo endereço**:
http://externalshop.dev/
